package project;



	import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;



public class BeatGoogle 
{
	
	private ArrayList<Keyword> keywords;
	public BeatGoogle(ArrayList<Keyword> keywords)
	{
		this.keywords = keywords;
		
	}
	
	
	public void search(String searchKeyword, PrintWriter webWriter) throws IOException
	{
		//HW10: call google
		GoogleQuery google = new GoogleQuery(/*java.net.URLEncoder.encode(searchKeyword,"utf-8")*/searchKeyword);
		//google result: name -> url
		HashMap<String, String> queryResults = google.query();
		
		//convert google results into root nodes
		//A root node can represent a tree, and also remember that
		//one google result represent one tree.
		ArrayList<Website> webRoots = new ArrayList<Website>();
		for(Entry<String, String> queryResult : queryResults.entrySet())
		{
			String url = queryResult.getValue().toLowerCase();
		
			if(!url.startsWith("http")&&url!=null)
			{
				url = "http://"+url;
			}
			String name = queryResult.getKey();
			webRoots.add(new Website(url, name));
			
		}
		
		
		
		for(Website rootWeb : webRoots)
		{
			//evaluate local score of all the root nodes and their children's local score
			//(their children will recursively be generated)
			rootWeb.evaluate(keywords);
			
			//HW6: calculate global score with post-order traversal
			calcGlobalScore(rootWeb);
		}
		
		
		//HW8: sort the root nodes with their global score by quick-sort algorithm
		ArrayList<Website> sortedWebRoots = doQuickSort(webRoots);
		
		//print result
		for(Website rootWeb : sortedWebRoots)
		{
			rootWeb.prettyPrint(searchKeyword);
		}
	}
	
	
	private void calcGlobalScore(Website webNode)
	{
		//calculate global score by post-order traversal
		for(Website child : webNode.getChildren())
		{
			calcGlobalScore(child);
		}
		
		webNode.globalScore = webNode.localScore;
		for(Website child : webNode.getChildren())
		{
			webNode.globalScore+=child.globalScore;
		}
		
	}
	
	
	private ArrayList<Website> doQuickSort(ArrayList<Website> webs)
	{
		if(webs.size()<2)
		{
			return webs;
		}
		
		ArrayList<Website> result = new ArrayList<Website>();
		
		int pivotIndex = webs.size()/2;
		Website pivotWeb = webs.get(pivotIndex);
		
		ArrayList<Website> lessList = new ArrayList<Website>();
		ArrayList<Website> equalList = new ArrayList<Website>();
		ArrayList<Website> greatList = new ArrayList<Website>();
		
		for(int i=0;i<webs.size();i++)
		{
			Website w = webs.get(i);
			if(w.globalScore >pivotWeb.globalScore)
			{
				greatList.add(w);
			}
			else if(w.globalScore <pivotWeb.globalScore)
			{
				lessList.add(w);
			}
			else 
			{
				equalList.add(w);
			}
		}
		
		result.addAll(doQuickSort(greatList));
		result.addAll(equalList);
		result.addAll(doQuickSort(lessList));
		
		return result;
		
		
		
	}
	
	

}


