package project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.net.URL;

import javax.swing.JOptionPane;
public class Run {
	public static void main(String[] args) throws IOException
	{
		
		//prepare some keywords that used to be criteria
		ArrayList<Keyword> keywords = new ArrayList<Keyword>();
		
		keywords.add(new Keyword("BB霜", 2.0));
		keywords.add(new Keyword("粉底液", 3.0));
		keywords.add(new Keyword("玻尿酸", 1.0));
		keywords.add(new Keyword("化妝水", 5.0));
		keywords.add(new Keyword("乳液", 4.0));
		keywords.add(new Keyword("保養", 50.0));
		keywords.add(new Keyword("化妝", 50.0));
		keywords.add(new Keyword("官方網站", 1000.0));
		keywords.add(new Keyword("官網", 1000.0));
		keywords.add(new Keyword("購物中心", -10.0));
		keywords.add(new Keyword("博客來", -10.0));
		keywords.add(new Keyword("購物網", -10.0));
		keywords.add(new Keyword("中文翻譯", -20.0));
		keywords.add(new Keyword("英漢", -20.0));
		
		BeatGoogle beatGoogle = new BeatGoogle(keywords);
		
		
		Scanner scanner = new Scanner(System.in);
		
		do
		{
			System.out.println("Please input search keyword and it's weight:");
			String searchKeyword = scanner.next();
			String weight=scanner.next();
			Double i = Double.valueOf(weight);
			keywords.add(new Keyword(searchKeyword, i));
			System.out.println("add success");
			
			String searchKeyword2=java.net.URLEncoder.encode(searchKeyword,"utf-8");
			
		    beatGoogle.search(searchKeyword2);
			
		}
		while(scanner.hasNextLine());
	}
}